Master:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/master/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/master)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/master/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/master)

Cafe of the Week:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/cafe-of-the-week/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/cafe-of-the-week)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/cafe-of-the-week/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/cafe-of-the-week)

Cafe Registration and Recommendation
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/tsaqif/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/tsaqif)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/tsaqif/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/tsaqif)

Cafe with Promo
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/dana2/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/dana2)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/dana2/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/dana2)

Cafe Nearby
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/kirana/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/kirana)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/kirana/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/kirana)

Cafe of All Time
[![pipeline status](https://gitlab.com/JustOneMoreLine/cup-bot/badges/mario/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/mario)
[![coverage report](https://gitlab.com/JustOneMoreLine/cup-bot/badges/mario/coverage.svg)](https://gitlab.com/JustOneMoreLine/cup-bot/-/commits/mario)
