package com.CupBotTeam.cupbot.service.apps.CafeOfAllTime;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CafeOfAllTimeImpl implements CafeOfAllTime {
    Twitter4JAdapter twitter;

    @Autowired
    public CafeOfAllTimeImpl(Twitter4JAdapter twitter) {
        this.twitter = twitter;
    }

    @Override
    public void ping(long twitterIdToPing) {
        twitter.sendDirectMessage(twitterIdToPing, "Here is out top 10 cafe of all time: X, Y, Z.");
    }

    @Override
    public void allTimeCafe() { }
}
