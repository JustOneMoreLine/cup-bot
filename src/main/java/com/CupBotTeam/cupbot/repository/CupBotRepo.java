package com.CupBotTeam.cupbot.repository;

import com.CupBotTeam.cupbot.core.databaseDTO.Cafe;
import com.CupBotTeam.cupbot.core.databaseDTO.CafeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CupBotRepo extends JpaRepository<Cafe, CafeId> {
}
