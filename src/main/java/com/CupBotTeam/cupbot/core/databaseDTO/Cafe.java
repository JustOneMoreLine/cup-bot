package com.CupBotTeam.cupbot.core.databaseDTO;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Entity
@Table(name = "cafe")
@SecondaryTable(name = "cafeAddress", pkJoinColumns = {
        @PrimaryKeyJoinColumn(name = "cafeOwnerTwitterId"),
        @PrimaryKeyJoinColumn(name = "cafeName"),
        @PrimaryKeyJoinColumn(name = "cafeOwnerPhone")
})
public class Cafe {
    @EmbeddedId
    private CafeId id;
    @Embedded
    private CafeAddress cafeAddress;
    @Column
    // a counter, counts how many times this cafe wins Cafe
    private long cafeOfTheWeekWins = 0;
    @Column
    private long cafeOfAllTimeRank = Long.MAX_VALUE;

    @Override
    public boolean equals(Object obj) {
        Cafe parse;
        if (obj.getClass().equals(Cafe.class)) {
            parse = (Cafe) obj;
            return this.equals(parse);
        }
        return false;
    }

    private boolean equals(Cafe obj) {
        if (
                this.id.equals(obj.getId())
                        && this.cafeAddress.equals(obj.getCafeAddress())
        ) {
            return true;
        }
        return false;
    }

    public CafeId getId() {
        return id;
    }

    public void setId(CafeId id) {
        this.id = id;
    }

    public CafeAddress getCafeAddress() {
        return cafeAddress;
    }

    public void setCafeAddress(CafeAddress cafeAddress) {
        this.cafeAddress = cafeAddress;
    }

    public long getCafeOfTheWeekWins() {
        return cafeOfTheWeekWins;
    }

    public void setCafeOfTheWeekWins(long cafeOfTheWeekWins) {
        this.cafeOfTheWeekWins = cafeOfTheWeekWins;
    }

    public long getCafeOfAllTimeRank() {
        return cafeOfAllTimeRank;
    }

    public void setCafeOfAllTimeRank(long cafeOfAllTimeRank) {
        this.cafeOfAllTimeRank = cafeOfAllTimeRank;
    }
}
