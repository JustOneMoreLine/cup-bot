package com.CupBotTeam.cupbot.core.databaseDTO;

import javax.persistence.Embeddable;

@Embeddable
public class CafeAddress {
    private String street;
    private String subDistrict; // INA translate: kelurahan
    private String district; // INA translate: kecamatan
    private String city;
    private String province; // INA translate: provinsi

    @Override
    public boolean equals(Object obj) {
        CafeAddress parse;
        if (obj.getClass().equals(CafeAddress.class)) {
            parse = (CafeAddress) obj;
            return this.equals(parse);
        }
        return false;
    }

    private boolean equals(CafeAddress obj) {
        if (
                this.street.equals(obj.getStreet())
                        && this.subDistrict.equals(obj.getSubDistrict())
                        && this.district.equals(obj.getDistrict())
                        && this.city.equals(obj.getCity())
                        && this.province.equals(obj.getProvince())
        ) {
            return true;
        }
        return false;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
