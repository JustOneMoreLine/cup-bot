package com.CupBotTeam.cupbot.core.databaseDTO;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class CafeId implements Serializable {
    private long cafeOwnerTwitterId;
    private String cafeName;
    private long cafeOwnerPhone;

    public long getCafeOwnerTwitterId() {
        return cafeOwnerTwitterId;
    }

    public void setCafeOwnerTwitterId(long cafeOwnerTwitterId) {
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
    }

    public String getCafeName() {
        return cafeName;
    }

    public void setCafeName(String cafeName) {
        this.cafeName = cafeName;
    }

    public long getCafeOwnerPhone() {
        return cafeOwnerPhone;
    }

    public void setCafeOwnerPhone(long cafeOwnerPhone) {
        this.cafeOwnerPhone = cafeOwnerPhone;
    }
}
