package com.CupBotTeam.cupbot.core.databaseDTO;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cafe_registration_log")
public class CafeRegistrationLog {
    @Id
    @Column
    private long dmId;
    @Column
    private long requesterTwitterId;
    @Column
    private CafeId cafeId;
    @Column
    private ZonedDateTime cafeCreationDate = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("GMT+7"));

    @Override
    public boolean equals(Object obj) {
        CafeRegistrationLog parse;
        if (obj.getClass().equals(CafeRegistrationLog.class)) {
            parse = (CafeRegistrationLog) obj;
            return this.equals(parse);
        }
        return false;
    }

    private boolean equals(CafeRegistrationLog obj) {
        if (
                this.dmId == obj.getDmId()
                        && this.requesterTwitterId == obj.getRequesterTwitterId()
                        && this.cafeId.equals(obj.getCafeId())
                        && this.cafeCreationDate.equals(obj.getCafeCreationDate())
        ) {
            return true;
        }
        return false;
    }

    public long getDmId() {
        return dmId;
    }

    public void setDmId(long dmId) {
        this.dmId = dmId;
    }

    public CafeId getCafeId() {
        return cafeId;
    }

    public void setCafeId(CafeId cafeId) {
        this.cafeId = cafeId;
    }

    public long getRequesterTwitterId() {
        return requesterTwitterId;
    }

    public void setCafeCreationDate(ZonedDateTime cafeCreationDate) {
        this.cafeCreationDate = cafeCreationDate;
    }

    public ZonedDateTime getCafeCreationDate() {
        return cafeCreationDate;
    }

    public void setRequesterTwitterId(long requesterTwitterId) {
        this.requesterTwitterId = requesterTwitterId;
    }
}
