package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.CafePromo.CafePromo;
import org.springframework.beans.factory.annotation.Autowired;


public class CafePromoHandler extends BaseKeywordHandler {
    CafePromo cafePromo;

    @Autowired
    public CafePromoHandler(CafePromo cafePromo, KeywordHandler endHandler) {
        this.cafePromo = cafePromo;
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            cafePromo.regisPromo(
                    message.getSenderID(),
                    getCafeNameFrom(message),
                    getTanggalFrom(message),
                    getKodePromoFrom(message),
                    getDescFrom(message)
            );
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(14, text.length()));
        if (keyword.equals("!regisPromo ")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 14) {
            return false; // no content, not even for keyword
        }
        String content = text.substring(14, text.length());
        String[] contentSplit = content.split("/");
        if (contentSplit.length == 4) {
            return true;
        }
        return false;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(14, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getTanggalFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private String getKodePromoFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[2];
    }

    private String getDescFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[3];
    }
}


/*package com.CupBotTeam.CupBot.service.KeywordHandler;

import com.CupBotTeam.CupBot.core.jsonTemplates.DirectMessageJSON;
import com.CupBotTeam.CupBot.service.apps.CafePromo.CafePromo;
import org.springframework.beans.factory.annotation.Autowired;

public class CafePromoHandler extends BaseKeywordHandler {
    CafePromo cafePromo;

    @Autowired
    public CafePromoHandler(CafePromo cafePromo, KeywordHandler endHandler){
        this.cafePromo = cafePromo;
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJSON message) {
        System.out.println("handle");
        System.out.println(keywordIsCorrectFrom(message));
        System.out.println(allRegistrationParamFilledFrom(message));
        if(keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)){
            System.out.println("True");
            cafePromo.regisPromo(
                    message.getSenderID(),
                    getCafeNameFrom(message),
                    getTanggalFrom(message),
                    getKodePromoFrom(message),
                    getDescFrom(message)
            );
            return true;
        } else {
            System.out.println("False");
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJSON message){
        String text = message.getText();
        String keyword = text.substring(0, Math.min(14, text.length()));
        if(keyword.equals("!cafeRegister ")) return true;
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJSON message){
        String text = message.getText();
        if(text.length() < 14) return false; // no content, not even for keyword
        String content = text.substring(14, text.length());
        String[] contentSplit = content.split("/");
        for(String x : contentSplit) System.out.println(x);
        if(contentSplit.length == 3) return true;
        return false;
    }

    private String[] contentSplit(String message){
        String removedKeywordFromText = message.substring(14, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJSON message) {
        return contentSplit(message.getText())[0];
    }

    private String getTanggalFrom(DirectMessageJSON message) {
        return contentSplit(message.getText())[1];
    }

    private String getKodePromoFrom(DirectMessageJSON message) {
        return contentSplit(message.getText())[2];
    }

    private String getDescFrom(DirectMessageJSON message) {
        return contentSplit(message.getText())[3];
    }
}*/



