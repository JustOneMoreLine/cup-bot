package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.CafeRegistration.CafeRegistration;
import org.springframework.beans.factory.annotation.Autowired;

public class CafeRegistrationHandler extends BaseKeywordHandler {
    CafeRegistration cafeRegistration;
    static String handlerKeyword = "!cafeRegister ";

    @Autowired
    public CafeRegistrationHandler(CafeRegistration cafeRegistration, KeywordHandler endHandler) {
        this.cafeRegistration = cafeRegistration;
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            cafeRegistration.registerCafe(
                    message.getSenderID(),
                    getCafeNameFrom(message),
                    getCafeLocationFrom(message),
                    getCafeOwnerFrom(message)
            );
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(14, text.length()));
        if (keyword.equals("!cafeRegister ")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 14) {
            return false; // no content, not even for keyword
        }
        String content = text.substring(14, text.length());
        String[] contentSplit = content.split("/");
        if (contentSplit.length == 3) {
            return true;
        }
        return false;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(14, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getCafeLocationFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private String getCafeOwnerFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[2];
    }
}
