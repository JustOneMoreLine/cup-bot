package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EndHandler extends BaseKeywordHandler {
    TestTwitter4J testTwitter4J;

    @Autowired
    public EndHandler(TestTwitter4J testTwitter4J) {
        this.testTwitter4J = testTwitter4J;
    }

    @Override
    public void setNextHandler(KeywordHandler nextHandler) {
        // do nothing
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        testTwitter4J.replyToDM(message.getSenderID());
        return true;
    }
}
