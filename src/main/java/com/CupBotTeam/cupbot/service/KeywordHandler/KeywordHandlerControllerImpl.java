package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class KeywordHandlerControllerImpl implements KeywordHandlerController {
    boolean isItNotSetUp = true;
    KeywordHandler firstHandler;
    private List<KeywordHandler> allHandler;

    @Autowired
    public KeywordHandlerControllerImpl(List<KeywordHandler> allHandler) {
        this.allHandler = allHandler;
    }

    @Override
    public void handle(DirectMessageJson message) {
        if (isItNotSetUp) {
            setUp();
        }
        firstHandler.handleRequest(message);
    }

    public void setUp() {
        for (int i = 0; i < allHandler.size() - 1; i++) {
            KeywordHandler aHandler = allHandler.get(i);
            KeywordHandler nextHandler = allHandler.get(i + 1);
            aHandler.setNextHandler(nextHandler);
        }
        firstHandler = allHandler.get(0);
        isItNotSetUp = false;
    }
}
