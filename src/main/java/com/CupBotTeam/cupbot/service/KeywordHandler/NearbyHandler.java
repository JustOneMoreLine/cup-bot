package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.cafenearby.CafeNearby;
import org.springframework.beans.factory.annotation.Autowired;

public class NearbyHandler extends BaseKeywordHandler {
    CafeNearby nearby;
    static String featureKeyword = "!nearby ";

    @Autowired
    public NearbyHandler(CafeNearby nearby, KeywordHandler endHandler) {
        this.nearby = nearby;
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            nearby.findNearby(message.getSenderID(),
                    getRegionFrom(message),
                    getCityFrom(message),
                    getProvinceFrom(message)
            );
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(featureKeyword.length(), text.length()));
        return keyword.equalsIgnoreCase(featureKeyword);
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < featureKeyword.length()) {
            return false;
        }
        String content = text.substring(featureKeyword.length(), text.length());
        String[] contentSplit = content.split("/");
        return contentSplit.length == 3;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(featureKeyword.length(), message.length());
        return removedKeywordFromText.split("/");
    }

    private String getRegionFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getCityFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private String getProvinceFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[2];
    }
}


