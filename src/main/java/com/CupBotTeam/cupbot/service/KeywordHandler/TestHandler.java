package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// Contoh Test Handler Concrete
@Component
public class TestHandler extends BaseKeywordHandler {

    TestTwitter4J testTwitter4J;

    @Autowired
    public TestHandler(TestTwitter4J testTwitter4J, KeywordHandler endHandler) {
        this.testTwitter4J = testTwitter4J;
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        String text = message.getText();
        // reading the keyword which is !tweetMe
        String keyword = text.substring(0, Math.min(8, text.length()));
        if (keyword.equals("!tweetMe")) {
            testTwitter4J.tweet(message.getSenderID(), text.substring(9, text.length()));
            return true;
        } else {
            return false;
        }
    }
}
