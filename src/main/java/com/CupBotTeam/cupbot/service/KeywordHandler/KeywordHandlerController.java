package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;

public interface KeywordHandlerController {

    public void handle(DirectMessageJson message);
}
