package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;

public interface KeywordHandler {

    // Untuk menset siapa handler berikutnya;
    public void setNextHandler(KeywordHandler nextHandler);

    // message akan mengandung seluruh string dari dm
    public void handleRequest(DirectMessageJson message);

}
