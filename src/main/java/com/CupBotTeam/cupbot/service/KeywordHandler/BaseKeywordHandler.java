package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;

public abstract class BaseKeywordHandler implements KeywordHandler {

    protected KeywordHandler next;
    protected KeywordHandler endHandler;

    // set next handler
    @Override
    public void setNextHandler(KeywordHandler nextHandler) {
        this.next = nextHandler;
    }

    @Override
    public void handleRequest(DirectMessageJson message) {

        if (handle(message)) {
            //do nothing
        } else {
            if (next != null) {
                next.handleRequest(message);
            } else {
                endHandler.handleRequest(message);
            }
        }
    }

    public abstract boolean handle(DirectMessageJson message);
}
