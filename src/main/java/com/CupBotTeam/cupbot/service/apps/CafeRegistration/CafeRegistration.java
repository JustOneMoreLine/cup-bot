package com.CupBotTeam.cupbot.service.apps.CafeRegistration;

public interface CafeRegistration {

    public void ping(long twitterIdToPing);

    public void registerCafe(
            long twitterIdWhoRegister,
            String cafeName,
            String cafeLocation,
            String cafeOwnerTwitter);
}
