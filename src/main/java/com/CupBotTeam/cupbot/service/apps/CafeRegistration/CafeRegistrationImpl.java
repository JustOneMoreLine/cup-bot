package com.CupBotTeam.cupbot.service.apps.CafeRegistration;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CafeRegistrationImpl implements CafeRegistration {
    Twitter4JAdapter twitter;

    @Autowired
    public CafeRegistrationImpl(Twitter4JAdapter twitter) {
        this.twitter = twitter;
    }

    @Override
    public void ping(long twitterIdToPing) {
        twitter.sendDirectMessage(twitterIdToPing, "You ask for Cafe Registration!");
    }

    @Override
    public void registerCafe(
            long twitterIdWhoRegister,
            String cafeName,
            String cafeLocation,
            String cafeOwnerTwitter) {
        // TODO add JPA before this
    }
}
