package com.CupBotTeam.cupbot.service.apps;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestTwitter4J {

    Twitter4JAdapter twitter;

    @Autowired
    public TestTwitter4J(Twitter4JAdapter adapter) {
        this.twitter = adapter;
    }

    public void tweet(long someoneWhoRequested, String textToTweet) {
        twitter.updateStatus(textToTweet);
        // log(someoneWhoRequested) if needed
    }

    public void replyToDM(long someonesIdToReply) {
        twitter.sendDirectMessage(someonesIdToReply, "Got your DM! I can't read it yet though, sorry!");
    }
}
