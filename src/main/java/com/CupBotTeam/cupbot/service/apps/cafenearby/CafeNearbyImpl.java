package com.CupBotTeam.cupbot.service.apps.cafenearby;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CafeNearbyImpl implements CafeNearby {
    Twitter4JAdapter twitter;

    @Autowired
    public CafeNearbyImpl(Twitter4JAdapter twitter) {
        this.twitter = twitter;
    }

    @Override
    public void ping(long twitterIdToPing) {
        twitter.sendDirectMessage(twitterIdToPing, "The best cafes in the area are Cafe X, Y, Z.");
    }

    @Override
    public void findNearby(long requesterId, String region, String city, String province) {

    }
}
