package com.CupBotTeam.cupbot.service.apps.CafePromo;

public interface CafePromo {
    public void ping(long twitterIdToPing);
    public void showCafeWithPromo();
    public void regisPromo(long twitterIdWhoRegister, String cafeName, String tanggal, String kodePromo, String desc);
}
