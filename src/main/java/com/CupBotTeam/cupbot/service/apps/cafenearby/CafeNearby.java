package com.CupBotTeam.cupbot.service.apps.cafenearby;

public interface CafeNearby {
    void ping(long twitterIdToPing);
    void findNearby(long requesterId, String region, String city, String province);
}

