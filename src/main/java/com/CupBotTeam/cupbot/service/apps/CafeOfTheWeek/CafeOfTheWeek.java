package com.CupBotTeam.cupbot.service.apps.CafeOfTheWeek;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapterImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class CafeOfTheWeek {
    Status cafeOfTheWeekNominationsTweet;
    Status cafeOfTheWeekTweet;

    List<String> cafeOfTheWeekNominations = new ArrayList<>();
    HashMap<String, Integer> cafeOfTheWeekPollCount;

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);

    Twitter4JAdapter twitter;

    public CafeOfTheWeek(Twitter4JAdapter twitter) {
        this.twitter = new Twitter4JAdapterImpl();
    }

    @Scheduled(cron = "0 0 7 ? * *", zone = "GMT+7")
    public void selectCafeOfTheWeekNomination() throws TwitterException {
        QueryResult latestCafeRecommendationTweets = getLatestCafeRecommendationTweets();
        String cafeOfTheDay = getCafeOfTheDay(latestCafeRecommendationTweets);
        if (cafeOfTheDay == null) {
            return;
        }
        cafeOfTheWeekNominations.add(cafeOfTheDay);
    }

    @Scheduled(cron = "0 0 8 ? * THU", zone = "GMT+7")
    public void postCafeOfTheWeekNominations() throws TwitterException {
        String cafeOfTheWeekNominationsTweetContent = generateCafeOfTheWeekNominationsTweetContent();
        moveNominationsToPollDictionary();
        cafeOfTheWeekNominationsTweet = twitter.updateStatus(cafeOfTheWeekNominationsTweetContent);
    }

    @Scheduled(cron = "0 0 8 ? * SAT", zone = "GMT+7")
    public void setCafeOfTheWeekWinner() throws TwitterException {
        processCafeOfTheWeekPolling();
        deleteCafeOfTheWeekNominationsTweet();
        String cafeOfTheWeek = decideCafeWithHighestPollCount();
        postCafeOfTheWeekWinner(cafeOfTheWeek);
    }

    private QueryResult getLatestCafeRecommendationTweets() throws TwitterException {
        Query query = new Query("from:hellocupbot CAFE RECOMMENDATION");
        query.setSince(ZonedDateTime.now(ZoneId.of("GMT+7")).minusDays(1).format(format));
        return twitter.search(query);
    }

    private String getCafeOfTheDay(QueryResult cafeRecommendationTweets) throws TwitterException {
        // Asumsi format tweet Cafe Recommendation
        /*
        CAFE RECOMMENDATION

        Nama Cafe : ...
        ...
        */
        int highestCount = 0;
        String cafeOfTheDay = null;
        for (Status tweet : cafeRecommendationTweets.getTweets()) {
            int thisTweetCount = tweet.getFavoriteCount() + tweet.getRetweetCount();
            if (thisTweetCount >= highestCount) {
                highestCount = thisTweetCount;
                cafeOfTheDay = tweet.getText().split("\n")[2].split(" : ")[1];
            }
        }

        return cafeOfTheDay;
    }

    private String generateCafeOfTheWeekNominationsTweetContent() {
        StringBuffer buffer = new StringBuffer("NOMINASI CAFE OF THE WEEK\nDukung cafe favoritmu"
                + " dengan cara me-reply tweet ini dengan nama cafe pilihanmu!\n\n");
        for (int i = 0; i < cafeOfTheWeekNominations.size(); i++) {
            buffer.append((i + 1) + ". " + cafeOfTheWeekNominations.get(i) + "\n");
        }

        return buffer.toString();
    }

    private void moveNominationsToPollDictionary() {
        cafeOfTheWeekPollCount = new HashMap<>();
        for (String cafeName : cafeOfTheWeekNominations) {
            cafeOfTheWeekPollCount.put(cafeName.toLowerCase(), 0);
            cafeOfTheWeekNominations.remove(cafeName);
        }
    }

    private void processCafeOfTheWeekPolling() throws TwitterException {
        Query query = new Query("to:hellocupbot since_id:" +
                cafeOfTheWeekNominationsTweet.getId());
        QueryResult results;

        while (query != null) {
            results = twitter.search(query);
            List<Status> tweetReplies = results.getTweets();
            doCountingPoll(tweetReplies);
            query = results.nextQuery();
        }
    }

    private void deleteCafeOfTheWeekNominationsTweet() throws TwitterException {
        twitter.destroyStatus(cafeOfTheWeekNominationsTweet.getId());
    }

    private String decideCafeWithHighestPollCount() {
        int maxPollCount = -1;
        String cafeOfTheWeekWinner = null;
        for (String cafeName : cafeOfTheWeekPollCount.keySet()) {
            if (cafeOfTheWeekPollCount.get(cafeName) > maxPollCount) {
                cafeOfTheWeekWinner = cafeName;
                maxPollCount = cafeOfTheWeekPollCount.get(cafeName);
            }
        }
        return cafeOfTheWeekWinner;
    }

    private void postCafeOfTheWeekWinner(String cafeOfTheWeek) throws TwitterException {
        cafeOfTheWeekTweet = twitter.updateStatus("PEMENANG CAFE OF THE WEEK\n\n"
                + "Selamat kepada " + cafeOfTheWeek + "!");
    }

    private void doCountingPoll(List<Status> tweetReplies) {
        for (Status poll : tweetReplies) {
            if (isValidPoll(poll)) {
                String cafeName = poll.getText().toLowerCase();
                cafeOfTheWeekPollCount.put(cafeName, cafeOfTheWeekPollCount.get(cafeName) + 1);
            }
        }
    }

    private boolean isValidPoll(Status poll) {
        return isReplyToNominationsTweet(poll) && isCafeOfTheWeekNominations(poll);
    }

    private boolean isReplyToNominationsTweet(Status poll) {
        return poll.getInReplyToStatusId() == cafeOfTheWeekNominationsTweet.getId();
    }

    private boolean isCafeOfTheWeekNominations(Status poll) {
        return cafeOfTheWeekPollCount.containsKey(poll.getText().toLowerCase());
    }

    public List<String> getNominationsList() {
        return cafeOfTheWeekNominations;
    }

    public Status getNominationsTweet() {
        return cafeOfTheWeekNominationsTweet;
    }

    public Status getCafeOfTheWeekTweet() {
        return cafeOfTheWeekTweet;
    }
}
