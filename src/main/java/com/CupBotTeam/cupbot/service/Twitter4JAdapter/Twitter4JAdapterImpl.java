package com.CupBotTeam.cupbot.service.Twitter4JAdapter;

import org.springframework.stereotype.Component;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterAPIConfiguration;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import java.util.Map;

@Component
public class Twitter4JAdapterImpl implements Twitter4JAdapter {

    Twitter twitter;

    public Twitter4JAdapterImpl() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("aiOQsUNsgK9EeueMoLtH0GWLO")
                .setOAuthConsumerSecret("p3Z6ds2jsDRyBeieHJBCjLKk791cthDJxoVS6OO39A34Yq9Hlj")
                .setOAuthAccessToken("1247058788189782016-zz9D9lnRtnMKve6KhQ6GSR8NhX7OMc")
                .setOAuthAccessTokenSecret("6CQFLn46VCvGdcV4mjkEtov47lk76G8MQPUwBPU93wJnb");
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        this.twitter = twitter;
    }

    public Twitter4JAdapterImpl(Twitter twitter) {
        this.twitter = twitter;
    }

    @Override
    public Status createFavorite(long id) {
        try {
            return twitter.createFavorite(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public User verifyCredentials() {
        try {
            return twitter.verifyCredentials();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status retweetStatus(long statusId) {
        try {
            return twitter.retweetStatus(statusId);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public QueryResult search(Query query) {
        try {
            return twitter.search(query);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status showStatus(long id) {
        try {
            return twitter.showStatus(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status destroyStatus(long id) {
        try {
            return twitter.destroyStatus(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status updateStatus(String status) {
        try {
            return twitter.updateStatus(status);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status updateStatus(StatusUpdate statusUpdate) {
        try {
            return twitter.updateStatus(statusUpdate);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessageList getDirectMessages(int count) {
        try {
            return twitter.getDirectMessages(count);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessageList getDirectMessages(int count, String cursor) {
        try {
            return twitter.getDirectMessages(count, cursor);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage showDirectMessage(long id) {
        try {
            return twitter.showDirectMessage(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage destroyDirectMessage(long id) {
        try {
            return twitter.destroyDirectMessage(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage sendDirectMessage(long recipientId, String text, long messageId) {
        try {
            return twitter.sendDirectMessage(recipientId, text, messageId);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage sendDirectMessage(long recipientId, String text) {
        try {
            return twitter.sendDirectMessage(recipientId, text);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage sendDirectMessage(String screenName, String text) {
        try {
            return twitter.sendDirectMessage(screenName, text);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ResponseList<User> lookupUsers(long[] ids) {
        try {
            return twitter.lookupUsers(ids);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ResponseList<User> lookupUsers(String[] screenNames) {
        try {
            return twitter.lookupUsers(screenNames);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public User showUser(long userId) {
        try {
            return twitter.showUser(userId);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public User showUser(String screenName) {
        try {
            return twitter.showUser(screenName);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ResponseList<User> searchUsers(String query, int page) {
        try {
            return twitter.searchUsers(query, page);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public TwitterAPIConfiguration getAPIConfiguration() {
        try {
            return twitter.getAPIConfiguration();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Map<String, RateLimitStatus> getRateLimitStatus() {
        try {
            return twitter.getRateLimitStatus();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Map<String, RateLimitStatus> getRateLimitStatus(String[] resources) {
        try {
            return twitter.getRateLimitStatus(resources);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }
}
