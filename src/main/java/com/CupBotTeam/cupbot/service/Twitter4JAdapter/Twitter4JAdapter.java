package com.CupBotTeam.cupbot.service.Twitter4JAdapter;

import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterAPIConfiguration;
import twitter4j.User;
import java.util.Map;

// Adapter buat twitter4j
public interface Twitter4JAdapter {

    public Status createFavorite(long id);

    public Status retweetStatus(long statusId);

    public QueryResult search(Query query);

    public Status showStatus(long id);

    public Status destroyStatus(long id);

    public Status updateStatus(String status);

    public Status updateStatus(StatusUpdate statusUpdate);

    public DirectMessageList getDirectMessages(int count);

    public DirectMessageList getDirectMessages(int count, String cursor);

    public DirectMessage showDirectMessage(long id);

    public DirectMessage destroyDirectMessage(long id);

    public DirectMessage sendDirectMessage(long recipientId, String text, long messageId);

    public DirectMessage sendDirectMessage(long recipientId, String text);

    public DirectMessage sendDirectMessage(String screenName, String text);

    public User verifyCredentials();

    public ResponseList<User> lookupUsers(long[] ids)
            ;
    public ResponseList<User> lookupUsers(String[] screenNames)
            ;
    public User showUser(long userId)
            ;
    public User showUser(String screenName)
            ;
    public ResponseList<User> searchUsers(String query,
                                          int page)
            ;
    public TwitterAPIConfiguration getAPIConfiguration()
            ;
    public Map<String, RateLimitStatus> getRateLimitStatus()
            ;
    public Map<String, RateLimitStatus> getRateLimitStatus(String[] resources)
            ;
}
