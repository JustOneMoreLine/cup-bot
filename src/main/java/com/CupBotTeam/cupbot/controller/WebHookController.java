package com.CupBotTeam.cupbot.controller;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.KeywordHandler.KeywordHandlerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/webhook")
public class WebHookController {
    KeywordHandlerController keywordHandlerControllerImpl;

    @Autowired
    public WebHookController(KeywordHandlerController keywordHandlerControllerImpl) {
        this.keywordHandlerControllerImpl = keywordHandlerControllerImpl;
    }

    @PostMapping(path = "/twitter")
    public ResponseEntity dataListen(@RequestBody DirectMessageJson messageJson) {
        if (messageJson.getDmID() != null &&
                messageJson.getSenderID() != null &&
                messageJson.getReceiverID() != null &&
                messageJson.getText() != null) {
            keywordHandlerControllerImpl.handle(messageJson);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/")
    public ResponseEntity hello() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
