package com.CupBotTeam.cupbot.service.apps.cafenearby;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;

public class CafeNearbyTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @InjectMocks
    CafeNearbyImpl serviceTested;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafeNearbyImpl(twitter4JAdapterImplMock);
    }

    @Test
    public void testServiceWorked() {
        long userDmId = 12345;
        serviceTested.ping(userDmId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(userDmId, "The best cafes in the area are Cafe X, Y, Z.");
    }


    // TODO: Add JPA and PostgreSQL to continue
}
