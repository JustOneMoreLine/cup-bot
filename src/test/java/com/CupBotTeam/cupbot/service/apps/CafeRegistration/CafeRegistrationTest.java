package com.CupBotTeam.cupbot.service.apps.CafeRegistration;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;

public class CafeRegistrationTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @InjectMocks
    CafeRegistrationImpl serviceTested;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafeRegistrationImpl(twitter4JAdapterImplMock);
    }

    @Test
    public void testServiceWorked() {
        long userDmId = 12345;
        serviceTested.ping(userDmId);
        verify(twitter4JAdapterImplMock)
                .sendDirectMessage(userDmId, "You ask for Cafe Registration!");
    }
    // TODO: Add JPA and PostgreSQL to continue
}
