package com.CupBotTeam.cupbot.service.apps.CafeOfTheWeek;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapterImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CafeOfTheWeekTest {

    @InjectMocks
    private CafeOfTheWeek cotw;

    private Twitter4JAdapter twitter = new Twitter4JAdapterImpl();

    @Test
    public void selectCafeOfTheWeekNominationTest() throws TwitterException, InterruptedException {
        Status status1 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe A");
        Status status2 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe B");
        Status status3 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe C");
        twitter.createFavorite(status2.getId());
        Thread.sleep(30000);

        cotw.selectCafeOfTheWeekNomination();
        assertEquals(cotw.getNominationsList().get(0),
                status2.getText().split("\n")[2].split(" : ")[1]);

        twitter.destroyStatus(status1.getId());
        twitter.destroyStatus(status2.getId());
        twitter.destroyStatus(status3.getId());
    }

    @Test
    public void postCafeOfTheWeekNominationsTest() throws TwitterException, InterruptedException {
        Status status1 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe A");
        Status status2 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe B");
        Status status3 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe C");

        twitter.createFavorite(status2.getId());
        Thread.sleep(30000);

        cotw.selectCafeOfTheWeekNomination();

        Status status4 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe D");
        Status status5 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe E");
        twitter.createFavorite(status4.getId());
        twitter.retweetStatus(status4.getId());
        twitter.retweetStatus(status5.getId());
        Thread.sleep(30000);

        cotw.selectCafeOfTheWeekNomination();
        cotw.postCafeOfTheWeekNominations();
        assertEquals(cotw.getNominationsTweet().getText(),
                "NOMINASI CAFE OF THE WEEK\nDukung cafe favoritmu dengan "
                        + "cara me-reply tweet ini dengan nama cafe pilihanmu!\n\n1. "
                        + status2.getText().split("\n")[2].split(" : ")[1] + "\n2. "
                        + status4.getText().split("\n")[2].split(" : ")[1]);

        twitter.destroyStatus(status1.getId());
        twitter.destroyStatus(status2.getId());
        twitter.destroyStatus(status3.getId());
        twitter.destroyStatus(status4.getId());
        twitter.destroyStatus(status5.getId());
        twitter.destroyStatus(cotw.getNominationsTweet().getId());
    }

    @Test
    public void setCafeOfTheWeekWinnerTest() throws TwitterException, InterruptedException {
        Status status1 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe A");
        Status status2 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe B");
        Status status3 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe C");

        twitter.createFavorite(status2.getId());
        Thread.sleep(30000);

        cotw.selectCafeOfTheWeekNomination();

        Status status4 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe D");
        Status status5 = twitter.updateStatus("CAFE RECOMMENDATION\n\nNama Cafe : Cafe E");
        twitter.createFavorite(status4.getId());
        twitter.retweetStatus(status4.getId());
        twitter.retweetStatus(status5.getId());
        Thread.sleep(30000);

        cotw.selectCafeOfTheWeekNomination();
        cotw.postCafeOfTheWeekNominations();
        Thread.sleep(30000);

        StatusUpdate update1 = new StatusUpdate(status2.getText().split("\n")[2].split(" : ")[1]);
        update1.setInReplyToStatusId(cotw.getNominationsTweet().getId());
        Status reply1 = twitter.updateStatus(update1);
        Thread.sleep(30000);

        cotw.setCafeOfTheWeekWinner();
        assertEquals(cotw.getCafeOfTheWeekTweet().getText(),
                "PEMENANG CAFE OF THE WEEK\n\nSelamat kepada "
                        + status2.getText().split("\n")[2].split(" : ")[1].toLowerCase() + "!");

        twitter.destroyStatus(status1.getId());
        twitter.destroyStatus(status2.getId());
        twitter.destroyStatus(status3.getId());
        twitter.destroyStatus(status4.getId());
        twitter.destroyStatus(status5.getId());
        twitter.destroyStatus(reply1.getId());
        twitter.destroyStatus(cotw.getCafeOfTheWeekTweet().getId());
    }
}
