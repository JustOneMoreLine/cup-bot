package com.CupBotTeam.cupbot.service.apps;

import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;

public class TestTwitter4JTest {

    @Mock
    Twitter4JAdapter twitter4JAdapterImpl;
    TestTwitter4J serviceTested;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new TestTwitter4J(twitter4JAdapterImpl);
    }

    @Test
    public void replyToDM() {
        long twitterIdOfSomeoneToReply = 12345;
        serviceTested.replyToDM(twitterIdOfSomeoneToReply);
        verify(twitter4JAdapterImpl).sendDirectMessage(
                twitterIdOfSomeoneToReply,
                "Got your DM! I can't read it yet though, sorry!");
    }

    @Test
    public void tweet() {
        long twitterIdOfSomeoneWhoRequestedTweet = 12345;
        String textToBeTweeted = "Subscribe to PewDiePie!";
        serviceTested.tweet(twitterIdOfSomeoneWhoRequestedTweet, textToBeTweeted);
        verify(twitter4JAdapterImpl).updateStatus(textToBeTweeted);
    }
}
