package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import com.CupBotTeam.cupbot.service.apps.cafenearby.CafeNearbyImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class NearbyHandlerTest {

    @InjectMocks
    NearbyHandler nearbyHandler;
    @Mock
    CafeNearbyImpl nearbyImplMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        nearbyHandler = new NearbyHandler(nearbyImplMock, endHandlerMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!nearby/Cinere/Depok/Jawa Barat");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!wrongkeyword/sCinere/Depok/Jawa Barat");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!nearby cinere depok");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock, never()).findNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock, never()).findNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock, never()).findNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }
}
