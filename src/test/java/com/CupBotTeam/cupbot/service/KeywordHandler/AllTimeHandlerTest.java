package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.service.apps.CafeOfAllTime.CafeOfAllTimeImpl;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AllTimeHandlerTest {

    @InjectMocks
    AllTimeHandler allTimeHandler;
    @Mock
    CafeOfAllTimeImpl allTimeImplMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        allTimeHandler = new AllTimeHandler();
    }
}
