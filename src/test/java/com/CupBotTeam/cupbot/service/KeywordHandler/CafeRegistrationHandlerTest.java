package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.apps.CafeRegistration.CafeRegistrationImpl;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafeRegistrationHandlerTest {

    @InjectMocks
    CafeRegistrationHandler cafeRegistrationHandler;
    @Mock
    CafeRegistrationImpl cafeRegistrationImplMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        cafeRegistrationHandler = new CafeRegistrationHandler(
                cafeRegistrationImplMock,
                endHandlerMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister starbucks bogor/Bogor/jack_meister");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!lubadubdub starbucks bogor/Bogor/jack_meister");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister starbucks bogor/Bogor");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }
}
