package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.verify;

public class KeywordHandlerTest {

    @InjectMocks
    KeywordHandlerControllerImpl keywordHandlerController;
    List<KeywordHandler> listOfHandlers;
    @Mock
    TestHandler testHandler;
    @Mock
    TestTwitter4J testHandlerService;
    @Mock
    KeywordHandler endHandler;
    @Mock
    Twitter4JAdapter twitter4JAdapterMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        testHandlerService = new TestTwitter4J(twitter4JAdapterMock);
        endHandler = new EndHandler(testHandlerService);
        testHandler = new TestHandler(testHandlerService, endHandler);
        listOfHandlers = new ArrayList<>();
        listOfHandlers.add(testHandler);
        keywordHandlerController = new KeywordHandlerControllerImpl(listOfHandlers);
    }

    public DirectMessageJson incomingDmExampleNoKnownKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("Hello");
        return result;
    }

    public DirectMessageJson incomingDmExampleKeywordTweetMe() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(6789);
        result.setReceiverID(151515);
        result.setText("!tweetMe Subscribe to PewDiePie!");
        return result;
    }

    @Test
    public void incomingDmWithNoKnownKeyword() {
        DirectMessageJson message = incomingDmExampleNoKnownKeyword();
        keywordHandlerController.handle(message);
        verify(twitter4JAdapterMock).sendDirectMessage(
                message.getSenderID(),
                "Got your DM! I can't read it yet though, sorry!");
    }

    @Test
    public void incomingDmKeywordTweetMe() {
        DirectMessageJson message = incomingDmExampleKeywordTweetMe();
        keywordHandlerController.handle(message);
        verify(twitter4JAdapterMock).updateStatus("Subscribe to PewDiePie!");
    }
}
