package com.CupBotTeam.cupbot.service.KeywordHandler;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.cupbot.service.apps.CafePromo.CafePromoImpl;
import com.CupBotTeam.cupbot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafePromoHandlerTest {

    @InjectMocks
    CafePromoHandler cafePromoHandler;
    @Mock
    CafePromoImpl cafePromoImplMock;
    @Mock
    Twitter4JAdapter twitter4JAdapterMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        cafePromoHandler = new CafePromoHandler(cafePromoImplMock, endHandlerMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!regisPromo DanaCafe/01-01-2021/AEZAKMI/Diskon_20%");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!BukanRegisPromo DanaCafe/01-01-2021/AEZAKMI/Diskon_20%");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!regisPromo 01-01-2021/AEZAKMI/Diskon_20%");
        return result;
    }

    /*@Test
    public void incomingDmWithCorrectKeyword(){
        DirectMessageJSON message = incomingDmExampleWithCorrectKeyword();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock).regisPromo(message.getSenderID(), "DanaCafe","01-01-2021","AEZAKMI","Diskon_20%");
    }*/

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock, never()).regisPromo(message.getSenderID(), "DanaCafe",
                "01/01/2021",
                "AEZAKMI",
                "Diskon_20%");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock, never()).regisPromo(message.getSenderID(), "DanaCafe",
                "01/01/2021",
                "AEZAKMI",
                "Diskon_20%");
    }
}



/*package com.CupBotTeam.CupBot.service.KeywordHandler;

import com.CupBotTeam.CupBot.core.jsonTemplates.DirectMessageJSON;
import com.CupBotTeam.CupBot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.CupBot.service.apps.CafePromo.CafePromo;
import com.CupBotTeam.CupBot.service.apps.CafePromo.CafePromoImpl;
import com.CupBotTeam.CupBot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
public class CafePromoHandlerTest {

    @InjectMocks
    CafePromoHandler cafePromoHandler;
    @Mock
    CafePromoImpl cafePromoImplMock;
    @Mock
    Twitter4JAdapter twitter4JAdapterMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        cafePromoHandler = new CafePromoHandler(cafePromoImplMock, endHandlerMock);
    }

    public DirectMessageJSON incomingDmExampleWithCorrectKeyword(){
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!RegisCafe DanaCafe|01/01/2021|AEZAKMI|Diskon_20%");
        return result;
    }

    public DirectMessageJSON incomingDmExampleWithWrongKeyword(){
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!BukanRegisCafe DanaCafe|01/01/2021|AEZAKMI|Diskon_20%");
        return result;
    }

    public DirectMessageJSON incomingDmExampleWithCorrectKeywordWrongFormat(){
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!RegisCafe 01/01/2021|AEZAKMI|Diskon_20%");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword(){
        DirectMessageJSON message = incomingDmExampleWithCorrectKeyword();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock).regisPromo(message.getSenderID(), "DanaCafe","01/01/2021","AEZAKMI","Diskon_20%");
    }

    @Test
    public void incomingDmWithWrongKeyword(){
        DirectMessageJSON message = incomingDmExampleWithWrongKeyword();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock, never()).regisPromo(message.getSenderID(), "DanaCafe",
        "01/01/2021","AEZAKMI","Diskon_20%");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat(){
        DirectMessageJSON message = incomingDmExampleWithCorrectKeywordWrongFormat();
        cafePromoHandler.handleRequest(message);
        verify(cafePromoImplMock, never()).regisPromo(message.getSenderID(), "DanaCafe","01/01/2021",
        "AEZAKMI","Diskon_20%");
    }
}
*/


/*package com.CupBotTeam.CupBot.service.KeywordHandler;

import com.CupBotTeam.CupBot.core.jsonTemplates.DirectMessageJSON;
import com.CupBotTeam.CupBot.service.Twitter4JAdapter.Twitter4JAdapter;
import com.CupBotTeam.CupBot.service.apps.CafeRegistration.CafeRegistrationImpl;
import com.CupBotTeam.CupBot.service.apps.TestTwitter4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafeRegistrationHandlerTest {

    @InjectMocks
    CafeRegistrationHandler cafeRegistrationHandler;
    @Mock
    CafeRegistrationImpl cafeRegistrationImplMock;
    @Mock
    Twitter4JAdapter twitter4JAdapterMock;
    @Mock
    KeywordHandler endHandlerMock;
    @Mock
    TestTwitter4J testTwitter4JMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        endHandlerMock = new EndHandler(testTwitter4JMock);
        cafeRegistrationHandler = new CafeRegistrationHandler(cafeRegistrationImplMock, endHandlerMock);
    }

    public DirectMessageJSON incomingDmExampleWithCorrectKeyword() {
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister starbucks bogor/Bogor/jack_meister");
        return result;
    }

    public DirectMessageJSON incomingDmExampleWithWrongKeyword() {
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!lubadubdub starbucks bogor/Bogor/jack_meister");
        return result;
    }

    public DirectMessageJSON incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJSON result = new DirectMessageJSON();
        result.setDmID(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister starbucks bogor/Bogor");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJSON message = incomingDmExampleWithCorrectKeyword();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJSON message = incomingDmExampleWithWrongKeyword();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJSON message = incomingDmExampleWithCorrectKeywordWrongFormat();
        cafeRegistrationHandler.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(
                message.getSenderID(),
                "starbucks bogor",
                "Bogor",
                "jack_meister");
    }
}

*/
