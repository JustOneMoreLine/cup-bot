package com.CupBotTeam.cupbot.controller;

import com.CupBotTeam.cupbot.core.jsonTemplates.DirectMessageJson;
import com.CupBotTeam.cupbot.service.KeywordHandler.KeywordHandlerController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = WebHookController.class)
public class WebHookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KeywordHandlerController keywordHandlerController;

    public String incomingJsonExample() {
        DirectMessageJson anObject = new DirectMessageJson();
        String result = "";
        anObject.setDmID(1234);
        anObject.setSenderID(5678);
        anObject.setReceiverID(191919);
        anObject.setText("Hello world!");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        try {
            result = ow.writeValueAsString(anObject);
        } catch (JsonProcessingException e) {
            System.out.println(e);
        }
        return result;
    }

    public String incomingJsonExampleWrong() {
        JSONObject anObject = new JSONObject();
        String result = "";
        anObject.appendField("HAHAHAHA", "WRONGO");
        anObject.appendField("M", 10231);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        try {
            result = ow.writeValueAsString(anObject);
        } catch (JsonProcessingException e) {
            System.out.println(e);
        }
        return result;
    }

    @Test
    public void accessingWebhookByGet() throws Exception {
        mockMvc.perform(get("/webhook/twitter"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void accessingWebhookByPost() throws Exception {
        mockMvc.perform(post("/webhook/twitter").contentType(MediaType.APPLICATION_JSON).content(incomingJsonExample()))
                .andExpect(status().isOk());
    }

    @Test
    public void getAccessingWebhookByPostInvalidJson() throws Exception {
        mockMvc.perform(post("/webhook/twitter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(incomingJsonExampleWrong()))
                .andExpect(status().isBadRequest());
    }
}
