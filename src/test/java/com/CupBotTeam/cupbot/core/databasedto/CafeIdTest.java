package com.CupBotTeam.cupbot.core.databasedto;

import com.CupBotTeam.cupbot.core.databaseDTO.CafeId;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CafeIdTest {
    CafeId cafeOwnerA;
    CafeId cafeOwnerB;

    public CafeId jackId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(12345);
        result.setCafeName("Starbuck");
        result.setCafeOwnerPhone(987);
        return result;
    }

    public CafeId tomId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(67890);
        result.setCafeName("Setarbak");
        result.setCafeOwnerPhone(6543);
        return result;
    }

    @Test
    public void existTwoDifferentCafeOwners() {
        cafeOwnerA = jackId();
        cafeOwnerB = tomId();
        assertNotEquals(cafeOwnerA, cafeOwnerB);
    }

    @Test
    public void testGetterSetter() {
        cafeOwnerA = jackId();
        assertEquals(cafeOwnerA.getCafeName(), "Starbuck");
        assertEquals(cafeOwnerA.getCafeOwnerTwitterId(), 12345);
        assertEquals(cafeOwnerA.getCafeOwnerPhone(), 987);
    }
}
