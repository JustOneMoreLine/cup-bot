package com.CupBotTeam.cupbot.core.databasedto;

import com.CupBotTeam.cupbot.core.databaseDTO.CafeAddress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CafeAddressTest {
    CafeAddress cafeAddress;

    @BeforeEach
    public void setUp() {
        cafeAddress = new CafeAddress();
        cafeAddress.setStreet("A Street No 1");
        cafeAddress.setSubDistrict("B Subdistrict");
        cafeAddress.setDistrict("C District");
        cafeAddress.setCity("D City");
        cafeAddress.setProvince("E Province");
    }

    @Test
    public void testGetterSetter() {
        assertEquals(cafeAddress.getStreet(), "A Street No 1");
        assertEquals(cafeAddress.getSubDistrict(), "B Subdistrict");
        assertEquals(cafeAddress.getDistrict(), "C District");
        assertEquals(cafeAddress.getCity(), "D City");
        assertEquals(cafeAddress.getProvince(), "E Province");
    }
}
