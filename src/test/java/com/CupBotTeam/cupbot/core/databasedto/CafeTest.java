package com.CupBotTeam.cupbot.core.databasedto;

import com.CupBotTeam.cupbot.core.databaseDTO.Cafe;
import com.CupBotTeam.cupbot.core.databaseDTO.CafeAddress;
import com.CupBotTeam.cupbot.core.databaseDTO.CafeId;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CafeTest {
    Cafe cafeX;
    Cafe cafeY;
    CafeId cafeOwnerA;
    CafeId cafeOwnerB;
    CafeAddress cafeAddressM;
    CafeAddress cafeAddressN;

    public CafeId jackId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(12345);
        result.setCafeName("Starbuck");
        result.setCafeOwnerPhone(987);
        return result;
    }

    public CafeId tomId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(67890);
        result.setCafeName("Setarbak");
        result.setCafeOwnerPhone(6543);
        return result;
    }

    public CafeAddress bogorAddress() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl Pakis Gunung No 25");
        result.setSubDistrict("Cilendek Timur");
        result.setDistrict("Bogor Barat");
        result.setCity("Kota Bogor");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress oregonAddress() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Pine Street No 26");
        result.setSubDistrict("Pines");
        result.setDistrict("Hollow Forrest");
        result.setCity("Hollow City");
        result.setProvince("Oregon");
        return result;
    }

    public Cafe newCafe(CafeId owner, CafeAddress address) {
        Cafe result = new Cafe();
        result.setId(owner);
        result.setCafeAddress(address);
        return result;
    }

    @Test
    public void existTwoDifferentCafes() {
        cafeOwnerA = jackId();
        cafeOwnerB = tomId();
        cafeAddressM = oregonAddress();
        cafeAddressN = bogorAddress();
        cafeX = newCafe(cafeOwnerA, cafeAddressM);
        cafeY = newCafe(cafeOwnerB, cafeAddressM);
        assertNotEquals(cafeX, cafeY);
    }

    @Test
    public void existTwoDifferentCafesOnTheSameAddress() {
        cafeOwnerA = jackId();
        cafeOwnerB = tomId();
        cafeAddressM = oregonAddress();
        cafeAddressN = oregonAddress();
        cafeX = newCafe(cafeOwnerA, cafeAddressM);
        cafeY = newCafe(cafeOwnerB, cafeAddressM);
        assertNotEquals(cafeX, cafeY);
    }

    @Test
    public void existOneCafeWithTwoAddress() {
        cafeOwnerA = jackId();
        cafeOwnerB = jackId();
        cafeAddressM = oregonAddress();
        cafeAddressN = bogorAddress();
        cafeX = newCafe(cafeOwnerA, cafeAddressM);
        cafeY = newCafe(cafeOwnerB, cafeAddressM);
        assertNotEquals(cafeX, cafeY);
    }

    @Test
    public void testGetterSetterForDefaultValues() {
        cafeOwnerA = jackId();
        cafeAddressM = oregonAddress();
        cafeX = newCafe(cafeOwnerA, cafeAddressM);
        assertEquals(cafeX.getCafeOfTheWeekWins(), 0);
        assertEquals(cafeX.getCafeOfAllTimeRank(), Long.MAX_VALUE);
        cafeX.setCafeOfAllTimeRank(9);
        assertEquals(cafeX.getCafeOfAllTimeRank(), 9);
        cafeX.setCafeOfTheWeekWins(10);
        assertEquals(cafeX.getCafeOfTheWeekWins(), 10);
    }
}
