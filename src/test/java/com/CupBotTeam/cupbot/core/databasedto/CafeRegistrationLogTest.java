package com.CupBotTeam.cupbot.core.databasedto;

import com.CupBotTeam.cupbot.core.databaseDTO.CafeId;
import com.CupBotTeam.cupbot.core.databaseDTO.CafeRegistrationLog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CafeRegistrationLogTest {
    CafeRegistrationLog cafeRegistrationLog;
    ZonedDateTime testDatetime;
    CafeId testId;

    @BeforeEach
    public void setUp() {
        testId = jackId();
        testDatetime = ZonedDateTime.now();
        cafeRegistrationLog = new CafeRegistrationLog();
        cafeRegistrationLog.setDmId(1234);
        cafeRegistrationLog.setRequesterTwitterId(5678);
        cafeRegistrationLog.setCafeId(testId);
    }

    public CafeId jackId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(12345);
        result.setCafeName("Starbuck");
        result.setCafeOwnerPhone(987);
        return result;
    }

    @Test
    public void testGetterSetter() {
        assertEquals(cafeRegistrationLog.getDmId(), 1234);
        assertEquals(cafeRegistrationLog.getCafeId(), testId);
        assertEquals(cafeRegistrationLog.getRequesterTwitterId(), 5678);
        LocalDate logDate = cafeRegistrationLog.getCafeCreationDate().toLocalDate();
        LocalDate testDate = testDatetime.toLocalDate();
        assertEquals(logDate, testDate);
    }
}
